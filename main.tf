resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/26"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name        = "mqr_vpc"
    Orgnization = "Merquri"
  }
}

resource "aws_subnet" "subnet_private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.0.0/27"

  tags = {
    Name        = "mqr_private_subnet"
    Orgnization = "Merquri"
  }
}

resource "aws_subnet" "subnet_public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.0.32/27"
  map_public_ip_on_launch = true
  tags = {
    Name        = "mqr_public_subnet"
    Orgnization = "Merquri"
  }
}

resource "aws_internet_gateway" "main_igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "mqr_igw"
  }
}

resource "aws_nat_gateway" "public_nat_gw" {
  allocation_id = aws_eip.my-eip.id
  subnet_id     = aws_subnet.subnet_public.id

  tags = {
    Name = "mqr_public_nat_gateway"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  # depends_on = [aws_internet_gateway.main_igw]
}

resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  key_name   = "myKey"       # Create a "myKey" to AWS!!
  public_key = tls_private_key.pk.public_key_openssh

  provisioner "local-exec" { # Create a "myKey.pem" to your computer!!
    command = "echo '${tls_private_key.pk.private_key_pem}' > ./myKey.pem"
  }
}

data "aws_ami" "ubuntu_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web_instance_public" {
  ami           = data.aws_ami.ubuntu_image.id
  subnet_id     = aws_subnet.subnet_public.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.kp.key_name

  vpc_security_group_ids = [
    aws_security_group.secgrp_public_instance.id
  ]

  tags = {
    Name = "mqr_instance_web_public"
  }
}

resource "aws_instance" "web_instance_private" {
  ami           = data.aws_ami.ubuntu_image.id
  subnet_id     = aws_subnet.subnet_private.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.kp.key_name

  user_data = <<-EOF
    #!/bin/bash
    mkdir -p ~/docker-nginx/html
    cd ~/docker-nginx/html
    echo "<h1>yeo yi</h1>" > ~/docker-nginx/html/index.html
    sudo apt update -y
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh ./get-docker.sh
    sudo docker pull nginx
    sudo docker run --name docker-nginx -p 80:80 -d -v ~/docker-nginx/html:/usr/share/nginx/html nginx
  EOF

  vpc_security_group_ids = [
    aws_security_group.secgrp_private_instance.id
  ]

  tags = {
    Name = "mqr_instance_web_private"
  }
}
