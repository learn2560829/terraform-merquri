resource "aws_security_group" "secgrp_public_instance" {
  description = "secgrp for public instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow inbound SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow inbound HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["118.189.0.0/16", "116.206.0.0/16", "223.25.0.0/16"]
  }

  egress {
    description = "Allow outbound all Ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "secgrp_mqr_public_instance"
  }
}


resource "aws_security_group" "secgrp_private_instance" {
  description = "secgrp for private instance"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow inbound SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow inbound HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["118.189.0.0/16", "116.206.0.0/16", "223.25.0.0/16", "10.0.0.0/26"]
  }

  egress {
    description = "Allow outbound all Ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "secgrp_mqr_private_instance"
  }
}

resource "aws_security_group" "secgrp_alb_public" {
  description = "secgrp for alb"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "Allow inbound all HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow outbound all Ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "secgrp_alb_internet"
  }
}

