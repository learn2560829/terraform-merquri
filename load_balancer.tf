resource "aws_lb_target_group" "alb_tg" {
  name     = "tg-check-http"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
}

resource "aws_lb_target_group_attachment" "alb_tg_add" {
  target_group_arn = aws_lb_target_group.alb_tg.arn
  target_id        = aws_instance.web_instance_private.id
  port             = 80
}

resource "aws_lb" "alb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.secgrp_alb_public.id]
  subnets            = [aws_subnet.subnet_private.id, aws_subnet.subnet_public.id]

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }
}