resource "aws_iam_user" "user1" {
  name = "test-user"
  path = "/system/"

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_user_policy_attachment" "read_only" {
  user       = aws_iam_user.user1.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}